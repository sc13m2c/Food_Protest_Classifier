import re

replacements = {'\n': ' ', '\r': ' '}

with open ('manualArticles.TXT') as oldfile, open('manualArticles2.TXT', 'w') as newfile:
	for line in oldfile:
		edited_line = re.sub( '\s+', ' ', line )
		newfile.write(edited_line)